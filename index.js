// console.log("Activity - ES6 Updates:");

const getCube = Math.pow(5,3);
console.log(`The cube of 5 is ${getCube} `)

let address = [245, "ABC street", "brgy EFG", "HIJ City", "Province of KLM", 3028];
const [ houseNumber, street, barangay, municipality, Province, zipCode ] = address;
console.log(`I live at ${houseNumber}, ${street}, ${barangay}, ${municipality}, ${Province}, ${zipCode}`);

const animal = { 
	animalName: "Lolong",
	animalSpecies: "saltwater crocodile",
	animalWeight: "1075 kgs",
	animalMeasurement: "20 ft 3 in"
}

let {animalName, animalSpecies, animalWeight, animalMeasurement} = animal;
console.log(`${animalName} was a ${animalSpecies}. He weighed at ${animalWeight} with a measurement of ${animalMeasurement}.`);

let array = [1, 2, 3, 4, 5,];

array.forEach((element) => console.log(element));

const initialValue = 0;
const total = array.reduce(
  (accumulator, currentValue) => accumulator + currentValue,
  initialValue
);

console.log(total);

class Dog{
	constructor(name, age, breed){
		this.dogName = name;
		this.dogAge = age;
		this.dogBreed = breed;

	}
}

let dog = new Dog("Frankie", 5, "Miniature Dachshund");
console.log(dog);
